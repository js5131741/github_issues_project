### Git clone the project

` git clone https://gitlab.com/js5131741/github_issues_project.git`

### Navigate to the directory and Install dependencies
`cd github_issues_project`
`npm install`

### To run your github

- Change repositary owner name, reponame, in script.js line:4
- Change private access token in app.js line: 7
- Run the html file in your brower

### To check my website

- Please click the below link.
  https://rangaraj-github-issues-project.netlify.app/

- Click the 'GITHUB ISSUES' header in the website, to check the github issues in the github directly.
