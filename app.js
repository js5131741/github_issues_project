/* eslint-disable no-unused-vars */
/* eslint-disable no-undef */

// function to do the api calls with different methods
async function CRUDoperation(apiUrl, method, data) {
  try {
    const accessToken = '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';
    const requestOptions = {
      method: method,
      headers: {
        Authorization: `Bearer ${accessToken}`,
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(data),
    };
    const response = await fetch(apiUrl, requestOptions);

    if (!response.ok) {
      throw new Error(`HTTP error! Status: ${response.status}`);
    }

    return await response.json();
  } catch (error) {
    console.error('Error in CRUDoperation:', error);
    throw error;
  }
}

//function to create a new issue
async function createIssue() {
  const title = document.getElementById('issue-title').value;
  const body = document.getElementById('issue-description').value;
  const data = { title, body };
  loadingScreen();
  let response;
  try {
    response = await CRUDoperation(apiUrl, 'POST', data);
  } catch (error) {
    console.error(error.message);
    removeLoadingScreen();
    showPopup('Error creating a new Issue', true);
    throw error;
  }
  console.log(response);
  removeLoadingScreen();
  showPopup('New Issue Created Successfully!');
  console.log('New issue created.');
  document.getElementById('issue-title').value = '';
  document.getElementById('issue-description').value = '';
  checkTitle();
  const activeButton = document.getElementById('activeIssuesBtn');
  activeButton.click();
}

// function to save changes
async function saveChanges(issueNumber) {
  const current_div = document.getElementById(`issue_id_${issueNumber}`);
  const new_title = current_div.querySelector('input');
  const new_description = current_div.querySelector('textarea');
  if (new_title.value.trim() !== '') {
    new_title.readOnly = true;
    new_description.readOnly = true;

    const title = new_title.value;
    const body = new_description.value;

    const data = { title, body };
    try {
      loadingScreen();
      await CRUDoperation(`${apiUrl}/${issueNumber}`, 'PATCH', data);
      await getIssues(apiUrl);
      removeLoadingScreen();
      showPopup('Successfully Updated! Please wait for some time to update.');
    } catch (error) {
      console.error('Error updating description:', error.message);
      showPopup('Error while updating issue!', true);
      removeLoadingScreen();
    }
  } else {
    showPopup('Please enter the Title of the Issue!', true);
  }
}

// close an issue
async function closeProcess(issueNumber) {
  const state = 'closed';
  loadingScreen();
  try {
    await CRUDoperation(`${apiUrl}/${issueNumber}`, 'PATCH', { state });
    await getIssues(apiUrl);
    removeLoadingScreen();
    showPopup('Closed Successfully! Please wait for some time to update.');
  } catch (error) {
    removeLoadingScreen();
    console.error('Error closing issue', error.message);
    showPopup('Error while closing the issue!', true);
  }
}

// reopen a closed issue
async function reopenProcess(issueNumber) {
  const state = 'open';
  loadingScreen();
  try {
    await CRUDoperation(`${apiUrl}/${issueNumber}`, 'PATCH', { state });
    await closedIssues(apiUrl);
    removeLoadingScreen();
    showPopup('Reopened Successfully! Please wait for some time to update.');
  } catch (error) {
    removeLoadingScreen();
    console.error('Error reopening issue', error.message);
    showPopup('Error while reopening the issue!', true);
  }
}

// function to get closed issues
async function closedIssues(apiUrl) {
  loadingScreen();
  const closeApiUrl = `${apiUrl}?state=closed`;
  await getIssues(closeApiUrl, 'close');
  removeLoadingScreen();
}

// main function
async function main() {
  const activeIssuesBtn = document.getElementById('activeIssuesBtn');
  activeIssuesBtn.addEventListener('click', async () => {
    toggleButton('activeIssuesBtn');
    await getIssues(apiUrl);
  });
  document.addEventListener('DOMContentLoaded', function () {
    // display the active issues when the browser reloads
    const activeButton = document.getElementById('activeIssuesBtn');
    activeButton.click();
  });
  const createIssueBtn = document.getElementById('createIssueBtn');
  createIssueBtn.addEventListener('click', () => {
    toggleButton('createIssueBtn');
    const issues = document.querySelector('.content');
    const createIssueContainer = document.querySelector('.create-issue');
    issues.classList.add('display-none');
    createIssueContainer.classList.remove('display-none');
  });

  const closedIssuesBtn = document.getElementById('closedIssuesBtn');
  closedIssuesBtn.addEventListener('click', async () => {
    toggleButton('closedIssuesBtn');
    await closedIssues(apiUrl);
  });
}

main();
