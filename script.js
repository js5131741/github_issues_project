/* eslint-disable no-unused-vars */
/* eslint-disable no-undef */

const owner = 'Rangaraj99';
const repoName = 'demo';
const apiUrl = `https://api.github.com/repos/${owner}/${repoName}/issues`;

// Color of the button and state based on the title value
function checkTitle() {
  const title = document.getElementById('issue-title').value;
  const newIssueBtn = document.getElementById('newIssueBtn');
  if (title.trim() !== '') {
    newIssueBtn.style.backgroundColor = '#238636';
    newIssueBtn.removeAttribute('disabled');
  } else {
    newIssueBtn.style.backgroundColor = '#8b9f64';
    newIssueBtn.setAttribute('disabled', 'true');
  }
}

// show popup message after a CRUD operation
function showPopup(message, e) {
  try {
    const popup = document.getElementById('popup');
    popup.style.backgroundColor = '#333333';
    popup.innerText = message;
    popup.style.display = 'block';
    if (e) {
      popup.style.backgroundColor = '#e75858';
    }

    setTimeout(() => {
      popup.style.display = 'none';
    }, 4000);
  } catch (error) {
    console.error('Error in showPopup:', error.message);
  }
}

// function to fetch issues with 'GET' method
async function getIssues(apiUrl, issueType) {
  let issues_data;
  try {
    loadingScreen();
    issues_data = await CRUDoperation(apiUrl, 'GET');
  } catch (error) {
    removeLoadingScreen();
    console.error('Error in getIssues:', error.message);
    showPopup('Cannot fetch issues at this time!', true);
  }
  const issues = document.querySelector('.content');
  const createIssueContainer = document.querySelector('.create-issue');
  issues.classList.remove('display-none');
  createIssueContainer.classList.add('display-none');
  issues.innerHTML = '';
  if (issues_data.length === 0) {
    const h4 = document.createElement('h4');
    h4.textContent = 'No Issues to Display!';
    issues.appendChild(h4);
  }
  issues_data.forEach((issue) => {
    const div = document.createElement('div');
    div.className = 'issue_content';
    div.setAttribute('id', `issue_id_${issue.number}`);

    const h3 = document.createElement('h3');
    h3.textContent = `Issue number : ${issue.number}`;
    div.appendChild(h3);

    const title_h5 = document.createElement('h5');
    title_h5.textContent = `Issue Title :`;
    div.appendChild(title_h5);

    const title_text = document.createElement('input');
    title_text.value = issue.title;
    title_text.readOnly = true;
    div.appendChild(title_text);

    const description_h5 = document.createElement('h5');
    description_h5.textContent = `Issue Description :`;
    div.appendChild(description_h5);

    const description_text = document.createElement('textarea');
    description_text.value = issue.body;
    description_text.readOnly = true;
    div.appendChild(description_text);

    if (issueType === 'close') {
      const reopen_button = document.createElement('button');
      reopen_button.setAttribute('class', 'update-buttons');
      reopen_button.innerText = 'Reopen Issue';
      reopen_button.setAttribute('onclick', `reopenProcess(${issue.number})`);
      div.appendChild(reopen_button);
    } else {
      const update_button = document.createElement('button');
      const close_button = document.createElement('button');
      update_button.setAttribute('class', 'update-buttons');
      close_button.setAttribute('class', 'update-buttons');
      close_button.setAttribute('id', 'close-button');
      update_button.setAttribute('onclick', `updateProcess(${issue.number})`);
      close_button.setAttribute('onclick', `closeProcess(${issue.number})`);
      update_button.innerText = 'Update Issue';
      close_button.innerText = 'Close Issue';
      div.appendChild(update_button);
      div.appendChild(close_button);
    }
    issues.appendChild(div);
    removeLoadingScreen();
  });
}

// function to update an issue if edited
async function updateProcess(issueNumber) {
  await getIssues(apiUrl);
  const current_div = document.getElementById(`issue_id_${issueNumber}`);
  const current_title = current_div.querySelector('input');
  const current_description = current_div.querySelector('textarea');
  current_title.readOnly = false;
  current_description.readOnly = false;

  const buttonsToRemove = current_div.querySelectorAll('button');
  buttonsToRemove.forEach((button) => {
    button.remove();
  });

  const save_changes = document.createElement('button');
  save_changes.setAttribute('id', 'save-button');
  save_changes.setAttribute('class', 'update-buttons');
  save_changes.innerText = 'Save Changes';
  const cancel_changes = document.createElement('button');
  cancel_changes.setAttribute('id', 'cancel-button');
  cancel_changes.setAttribute('class', 'update-buttons');
  cancel_changes.innerText = 'Cancel';

  current_div.appendChild(save_changes);
  current_div.appendChild(cancel_changes);
  try {
    save_changes.addEventListener(
      'click',
      async () => await saveChanges(issueNumber),
    );

    cancel_changes.addEventListener('click', async () => {
      // cancel changes midway
      await getIssues(apiUrl);
    });
  } catch (error) {
    console.error('Error in updateProcess:', error.message);
  }
}

// show the active button with green color
function toggleButton(clickedButton) {
  const buttonList = ['createIssueBtn', 'activeIssuesBtn', 'closedIssuesBtn'];
  buttonList.forEach((button) => {
    if (clickedButton !== button) {
      document.getElementById(button).style.backgroundColor = '#333333';
    } else {
      document.getElementById(button).style.backgroundColor = '#3cb043';
    }
  });
}

function loadingScreen() {
  const loader = document.querySelector('.loader');
  loader.classList.remove('display-none');
}

function removeLoadingScreen() {
  const loader = document.querySelector('.loader');
  loader.classList.add('display-none');
}
